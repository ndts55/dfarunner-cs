﻿using System;
using System.IO;

namespace DFARunner
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            foreach (var s in args)
            {
                Console.WriteLine($"arg: {s}");
            }

            if (args.Length < 1) return;
            var path = args[0];
            var source = File.ReadAllText(path);
            var dfa = new Parser().Parse(source);

            if (!dfa.IsValid)
            {
                Console.WriteLine("dfa invalid");
                return;
            }

            Console.WriteLine("input word to check if dfa accepts it");
            while (true)
            {
                Console.Write("> ");
                var input = Console.ReadLine();
                if (!(input is null)
                    && input.Equals("exit", StringComparison.InvariantCultureIgnoreCase))
                    break;
                Console.WriteLine(dfa.Accepts(input)
                    ? "yes"
                    : "no");
            }
            
            Console.WriteLine("bye");
        }
    }
}