using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DFARunner.Extensions;
using DFARunner.Models;
using Optional;
using Optional.Unsafe;

namespace DFARunner
{
    public class Parser
    {
        private readonly Dictionary<string, State> _acceptStates;
        private readonly Dictionary<string, State> _states;
        private readonly HashSet<char> _alphabet;

        public Parser()
        {
            _states = new Dictionary<string, State>();
            _acceptStates = new Dictionary<string, State>();
            _alphabet = new HashSet<char>();
        }

        public IReadOnlyDictionary<string, State> States => _states;

        public IReadOnlyDictionary<string, State> AcceptStates => _acceptStates;

        public IReadOnlyCollection<char> Alphabet => _alphabet;

        public Option<State> Initial { get; private set; }

        public Dfa Parse(string source)
        {
            source
                .SplitDeclarations()
                .RemoveEmptyOrWhitespace()
                .ToList()
                .ForEach(l =>
                {
                    var line = l.RemoveWhitespace();
                    if (line.StartsWith("init:")) ParseInit(line);
                    else if (line.StartsWith("accept:")) ParseAccept(line);
                    else ParseTransition(line);
                });

            var initial = Initial.Match(
                s => s,
                () => throw new Exception("initial state not defined")
            );

            var states = States.Values.ToImmutableArray();
            var accepts = AcceptStates.Values.ToImmutableArray();
            var alphabet = Alphabet.ToImmutableHashSet();
            
            return new Dfa(alphabet, states, accepts, initial);
        }

        public void ParseInit(string line)
        {
            Initial.MatchSome(s =>
                throw new Exception($"init is already defined as state with name {s.Name}"));

            var parts = line
                .RemoveWhitespace()
                .Split(':')
                .RemoveEmptyOrWhitespace();

            if (parts.Length != 2)
                throw new SyntaxErrException(line);

            var newState = State.Create(parts[1]);
            if (!newState.HasValue)
                throw new SyntaxErrException(line);

            if (States.TryGetValue(newState.ValueOrDefault().Name, out var state))
            {
                Initial = state.Some();
            }
            else
            {
                Initial = newState;
                newState.MatchSome(s => _states.Add(s.Name, s));
            }
        }

        public void ParseAccept(string line)
        {
            var parts = line
                .RemoveWhitespace()
                .Split(':')
                .RemoveEmptyOrWhitespace();

            if (parts.Length != 2)
                throw new SyntaxErrException(line);

            parts[1]
                .Split(',')
                .RemoveEmptyOrWhitespace()
                .Select(State.Create)
                .ToList()
                .ForEach(o =>
                    o.Match(s =>
                        {
                            _states.Add(s.Name, s);
                            _acceptStates.Add(s.Name, s);
                        },
                        () => throw new SyntaxErrException(line)));
        }

        public void ParseTransition(string line)
        {
            var parts = line
                .RemoveWhitespace()
                .Split(',')
                .RemoveEmptyOrWhitespace();

            if (parts.Length != 3)
                throw new SyntaxErrException(line);

            var currentName = parts[0];
            if (!States.TryGetValue(currentName, out var current))
            {
                current = State.Create(currentName)
                    .ValueOrThrow(new SyntaxErrException(line));
                _states.Add(current.Name, current);
            }

            var nextName = parts[2];
            if (!States.TryGetValue(nextName, out var next))
            {
                next = State.Create(nextName)
                    .ValueOrThrow(new SyntaxErrException(line));
                _states.Add(next.Name, next);
            }
            
            var input = parts[1].ToChar()
                .ValueOrThrow(new SyntaxErrException(line));

            _alphabet.Add(input);

            current.AddTransition(input, next);
        }
    }
}