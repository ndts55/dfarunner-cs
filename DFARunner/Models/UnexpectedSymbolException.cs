using System;

namespace DFARunner.Models
{
    public class UnexpectedSymbolException : Exception
    {
        public UnexpectedSymbolException(char symbol) : base($"unexpected symbol: {symbol}")
        {}
    }
}