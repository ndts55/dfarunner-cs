using System;

namespace DFARunner.Models
{
    public class SyntaxErrException : Exception
    {
        public SyntaxErrException(string line) : base($"syntax error: {line}")
        {
        }
    }
}