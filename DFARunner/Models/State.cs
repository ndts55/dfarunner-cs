using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DFARunner.Extensions;
using Optional;
using Optional.Collections;

namespace DFARunner.Models
{
    public class State : IEquatable<State>
    {
        private readonly Dictionary<char, State> _transitions;

        private State(string name)
        {
            Name = name;
            _transitions = new Dictionary<char, State>();
        }

        public string Name { get; }

        public IReadOnlyDictionary<char, State> Transitions => _transitions;

        public bool Equals(State other)
        {
            return !(other is null)
                   && (ReferenceEquals(this, other)
                       || Name.Equals(other.Name)
                       && TransitionsEquals(other.Transitions));
        }

        public static Option<State> Create(string name)
        {
            return name.Length == 0 || name.ContainsAny(new[] {':', ';', ','})
                ? Option.None<State>()
                : new State(name).Some();
        }

        public void AddTransition(char input, State next)
        {
            _transitions.Add(input, next);
        }

        public State Compute(string word)
        {
            return word.SplitFirst().Match(
                p =>
                {
                    var (h, t) = p;
                    return Transitions.GetValueOrNone(h)
                        .Match(
                            next => next.Compute(t),
                            () => throw new UnexpectedSymbolException(h)
                        );
                },
                () => this
            );
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as State);
        }

        private bool TransitionsEquals(IReadOnlyDictionary<char, State> others) =>
            others.Count == Transitions.Count
            && Transitions.Keys.Aggregate(true, (current, k) => current && others.ContainsKey(k));

        public override int GetHashCode()
        {
            unchecked
            {
                var nh = Name.GetHashCode();
                return (397 + 397 * nh * 397 + nh) ^ 521;
            }
        }

        public override string ToString()
        {
            var transitions = new StringBuilder();
            foreach (var (k, v) in Transitions)
            {
                transitions.Append($"{k} => {v.Name}; ");
            }

            return $"Name: {Name}, Transitions:" + transitions;
        }
    }
}