using System;
using System.Collections.Immutable;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Optional;

namespace DFARunner.Models
{
    public class Dfa : IEquatable<Dfa>
    {
        public ImmutableArray<State> States { get; }
        public ImmutableArray<State> AcceptStates { get; }
        public ImmutableHashSet<char> Alphabet { get; }
        public State Initial { get; }

        private Option<bool> IsValidOption { get; set; }

        public bool IsValid
        {
            get
            {
                return IsValidOption.Match(
                    b => b,
                    () =>
                    {
                        var valid = Validate();
                        IsValidOption = valid.Some();
                        return valid;
                    }
                );
            }
        }

        public Dfa(ImmutableHashSet<char> alphabet, ImmutableArray<State> states, ImmutableArray<State> acceptStates, 
        State initial)
        {
            Alphabet = alphabet;
            States = states;
            AcceptStates = acceptStates;
            Initial = initial;

            IsValidOption = Option.None<bool>();
        }

        public bool Accepts(string input)
        {
            var finalState = Initial.Compute(input);
            return AcceptStates.Contains(finalState);
        }

        private bool Validate()
        {
            return !(Initial is null)
                   && AcceptStates.Length != 0
                   && States.Length != 0
                   && States.Contains(Initial)
                   && AcceptStates.Aggregate(true,
                       (acc, s) => acc && States.Contains(s))
                   && States.Aggregate(true,
                       (acc, s) => acc && s.Transitions.Keys.SequenceEqual(Alphabet));
        }

        public bool Equals(Dfa other) =>
            !(other is null)
            && States.Equals(other.States)
            && AcceptStates.Equals(other.AcceptStates)
            && Initial.Equals(other.Initial);

        public override bool Equals(object obj) =>
            Equals(obj as Dfa);

        public override int GetHashCode()
        {
            unchecked
            {
                return States.GetHashCode() * AcceptStates.GetHashCode() ^ Initial.GetHashCode() * 397 * 521;
            }
        }

        public override string ToString()
        {
            var bob = new StringBuilder();
            bob.AppendLine($"Initial: {Initial}");
            bob.AppendLine("States:");

            foreach (var state in States)
            {
                bob.AppendLine(state.ToString());
            }

            bob.AppendLine("\nAcceptStates:");

            foreach (var state in AcceptStates)
            {
                bob.AppendLine(state.ToString());
            }

            return bob.ToString();
        }
    }
}