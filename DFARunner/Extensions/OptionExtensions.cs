using System;
using Optional;
using Optional.Unsafe;

namespace DFARunner.Extensions
{
    public static class OptionExtensions
    {
        public static TV ValueOrThrow<TV, TE>(this Option<TV> option, TE exception) 
            where TE : Exception
        {
            if (option.HasValue) return option.ValueOrDefault();
            throw exception;
        }
    }
}