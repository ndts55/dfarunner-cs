using System.Linq;
using System.Text.RegularExpressions;
using Optional;

namespace DFARunner.Extensions
{
    public static class StringExtensions
    {
        public static string RemoveWhitespace(this string str)
        {
            return Regex.Replace(str, @"\s+", string.Empty);
        }

        public static string[] RemoveEmptyOrWhitespace(this string[] strings)
        {
            return strings.Where(s => s.RemoveWhitespace().IsNotEmpty()).ToArray();
        }

        private static bool IsNotEmpty(this string str)
        {
            return str.Length != 0;
        }

        public static bool ContainsAny(this string str, char[] cs)
        {
            return str.IndexOfAny(cs) != -1;
        }

        public static Option<char> ToChar(this string str)
        {
            if (str is null || str.Length > 1)
                return Option.None<char>();

            return Option.Some(char.Parse(str));
        }

        public static string[] SplitDeclarations(this string str) =>
            str.Split('\n', ';');

        public static Option<(char, string)> SplitFirst(this string str) =>
            str.Length < 1
                ? Option.None<(char, string)>()
                : (char.Parse(str.Substring(0, 1)), str.Substring(1)).Some();
    }
}