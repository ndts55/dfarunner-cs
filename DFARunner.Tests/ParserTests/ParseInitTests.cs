using System;
using System.Collections.Generic;
using DFARunner.Models;
using DFARunner.Tests.Helpers;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.ParserTests
{
    public class ParseInitTests
    {
        public ParseInitTests()
        {
            _parser = new Parser();
        }

        private readonly Parser _parser;

        [Theory]
        [InlineData("init: ")]
        [InlineData("init: q1 : q2")]
        [InlineData("init: q1, q2")]
        public void ParseInit_throws_exception_on_syntax_error(string line)
        {
            Should.Throw<SyntaxErrException>(() => _parser.ParseInit(line))
                .Message
                .ShouldBe($"syntax error: {line}");
        }

        [Fact]
        public void ParseInit_adds_correct_state_to_States()
        {
            _parser.ParseInit("init: q0");
            var expected = new KeyValuePair<string, State>("q0", State.Create("q0").AssertSome());
            _parser.States.ShouldContain(expected);
        }

        [Fact]
        public void ParseInit_sets_Initial_state()
        {
            _parser.ParseInit("init: q0");
            _parser.Initial.ShouldBeSome(State.Create("q0").AssertSome());
        }

        [Fact]
        public void ParseInit_throws_exception_when_defining_init_more_than_once()
        {
            _parser.ParseInit("init: q0");
            Should.Throw<Exception>(() => _parser.ParseInit("init: q3"))
                .Message
                .ShouldBe("init is already defined as state with name q0");
        }
    }
}