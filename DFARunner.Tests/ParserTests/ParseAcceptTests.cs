using System.Collections.Generic;
using DFARunner.Models;
using DFARunner.Tests.Helpers;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.ParserTests
{
    public class ParseAcceptTests
    {
        public ParseAcceptTests()
        {
            _parser = new Parser();
        }

        private readonly Parser _parser;

        [Theory]
        [InlineData("accept: ")]
        [InlineData("accept: q1: q2")]
        [InlineData("accept: q1; q2")]
        public void ParseAccept_throws_exception_on_syntax_error(string line)
        {
            Should.Throw<SyntaxErrException>(() => _parser.ParseAccept(line))
                .Message
                .ShouldBe($"syntax error: {line}");
        }

        [Fact]
        public void ParseAccept_accumulates_accepting_states()
        {
            _parser.ParseAccept("accept: q1");
            _parser.ParseAccept("accept: q2");

            var expected = new Dictionary<string, State>
            {
                {"q1", State.Create("q1").AssertSome()},
                {"q2", State.Create("q2").AssertSome()}
            };

            _parser.States.ShouldBe(expected);
            _parser.AcceptStates.ShouldBe(expected);
        }

        [Fact]
        public void ParseAccept_adds_states_to_States_and_AcceptStates()
        {
            _parser.ParseAccept("accept: q1, q2");
            var expected = new Dictionary<string, State>
            {
                {"q1", State.Create("q1").AssertSome()},
                {"q2", State.Create("q2").AssertSome()}
            };

            _parser.States.ShouldBe(expected);
            _parser.AcceptStates.ShouldBe(expected);
        }
    }
}