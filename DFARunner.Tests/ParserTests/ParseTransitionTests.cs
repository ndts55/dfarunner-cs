using System.Collections.Generic;
using DFARunner.Models;
using DFARunner.Tests.Helpers;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.ParserTests
{
    public class ParseTransitionTests
    {
        public ParseTransitionTests()
        {
            _parser = new Parser();
        }

        private readonly Parser _parser;

        [Fact]
        public void ParseTransition_adds_states_to_States()
        {
            _parser.ParseTransition("q1, a, q2");

            var expected = new[] {"q1", "q2"};

            _parser.States.Keys.ShouldBe(expected);
        }

        [Fact]
        public void ParseTransition_adds_transition_to_states()
        {
            _parser.ParseTransition("q1, a, q2");

            var expectedTransitions = new Dictionary<char, State>
            {
                {'a', _parser.States["q2"]}
            } as IReadOnlyDictionary<char, State>;

            _parser.States.ShouldSatisfyAll(
                ss => ss["q1"].Transitions.ShouldBe(expectedTransitions),
                ss => ss["q2"].Transitions.ShouldBeEmpty()
            );
        }

        [Theory]
        [InlineData("q1, ")]
        [InlineData("q1, a, ")]
        [InlineData("q1, a, b, q2")]
        [InlineData("q;2, a, q3")]
        [InlineData("q1, ac, q2")]
        public void ParseTransition_throws_when_syntax_wrong(string line)
        {
            Should.Throw<SyntaxErrException>(() => _parser.ParseTransition(line))
                .Message
                .ShouldBe($"syntax error: {line}");
        }

        [Fact]
        public void ParseTransition_adds_multiple_transitions_to_state()
        {
            _parser.ParseTransition("q1, a, q2");
            _parser.ParseTransition("q1, b, q3");
            _parser.ParseTransition("q1, c, q4");

            _parser.States["q1"].Transitions.ShouldSatisfyAll(
                ts => ts['a'].ShouldBe(_parser.States["q2"]),
                ts => ts['b'].ShouldBe(_parser.States["q3"]),
                ts => ts['c'].ShouldBe(_parser.States["q4"])
            );
        }

        [Fact]
        public void ParseTransition_adds_input_to_Alphabet()
        {
            _parser.ParseTransition("q1, a, q2");
            _parser.Alphabet.ShouldContain('a');
        }

        [Fact]
        public void ParseTransitions_adds_input_to_Alphabet_only_once()
        {
            _parser.ParseTransition("q1, a, q2");
            _parser.ParseTransition("q2, a, q1");

            _parser.Alphabet.ShouldSatisfyAll(
                c => c.Count.ShouldBe(1),
                c => c.ShouldContain('a'));
        }
    }
}