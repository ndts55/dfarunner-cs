using System.Collections.Immutable;
using DFARunner.Models;
using DFARunner.Tests.Helpers;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.ParserTests
{
    public class ParseTests
    {
        [Fact]
        public void Parse_returns_correct_DFA()
        {
            const string source = "init:q0;accept:q1;q0,a,q1;q1,a,q0";

            var q0 = State.Create("q0").AssertSome();
            var q1 = State.Create("q1").AssertSome();

            q0.AddTransition('a', q1);
            q1.AddTransition('a', q0);

            var expectedStates = ImmutableList<State>.Empty.AddRange(new[] {q0, q1});
            var expectedAccepts = ImmutableList<State>.Empty.Add(q1);


            new Parser().Parse(source).ShouldSatisfyAll(
                dfa => dfa.States.ShouldBe(expectedStates),
                dfa => dfa.AcceptStates.ShouldBe(expectedAccepts),
                dfa => dfa.Initial.ShouldBe(q0)
            );
        }
    }
}