using System;
using DFARunner.Extensions;
using Optional;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.Extensions
{
    public class OptionExtensionsTests
    {
        [Fact]
        public void ValueOrThrow_throws_on_none()
        {
            var none = Option.None<string>();
            var exception = new Exception("NO!");
            Should.Throw<Exception>(() => none.ValueOrThrow(exception))
                .ShouldBe(exception);
        }

        [Fact]
        public void ValueOrThrow_returns_value_on_some()
        {
            const string str = "string";
            var some = str.Some();
            some.ValueOrThrow(new Exception()).ShouldBe(str);
        }
    }
}