using System.Collections.Generic;
using DFARunner.Extensions;
using DFARunner.Tests.Helpers;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.Extensions
{
    public class StringExtensionsTests
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public static readonly IEnumerable<object[]> CaTestData =
            new[]
            {
                new object[] {"str!ng", new[] {'!'}, true},
                new object[] {"string", new char[0], false},
                new object[] {"string", new[] {'\n'}, false}
            };

        [Theory]
        [MemberData(nameof(CaTestData))]
        public void ContainsAny_returns_correct_result(string str, char[] cs, bool expected)
        {
            str.ContainsAny(cs).ShouldBe(expected);
        }

        [Theory]
        [InlineData("A", 'A')]
        [InlineData("1", '1')]
        [InlineData("\n", '\n')]
        public void ToChar_returns_some_char(string input, char expected)
        {
            input.ToChar().ShouldBeSome(expected);
        }

        [Theory]
        [InlineData(null)]
        [InlineData("ab")]
        [InlineData("longer than one char")]
        public void ToChar_returns_none(string input)
        {
            input.ToChar().ShouldBeNone();
        }

        [Fact]
        public void RemoveEmptyOrWhitespace_removes_empty_strings_from_string_array()
        {
            new[] {"string", string.Empty, "\n\t\r"}
                .RemoveEmptyOrWhitespace()
                .ShouldBe(new[] {"string"});
        }

        [Theory]
        [InlineData("   hel   lo   ", "hello")]
        [InlineData("    one    ", "one")]
        [InlineData("\n\r\t", "")]
        public void RemoveWhiteSpace_returns_expected_string(string input, string expected) =>
            input.RemoveWhitespace().ShouldBe(expected);

        [Fact]
        public void SplitDeclarations_splits_semicolon_and_newline()
        {
            "one;two\nthree"
                .SplitDeclarations()
                .ShouldBe(new[] {"one", "two", "three"});
        }

        [Fact]
        public void SplitFirst_returns_none_when_empty() =>
            string.Empty.SplitFirst().ShouldBeNone();

        // ReSharper disable once MemberCanBePrivate.Global
        public static readonly IEnumerable<object[]> SfData =
            new[]
            {
                new object[] {"string", ('s', "tring")},
                new object[] {"s", ('s', string.Empty)},
                new object[] {"nd", ('n', "d")}
            };

        [Theory, MemberData(nameof(SfData))]
        public void SplitFirst_returns_some(string input, (char, string) expected) =>
            input.SplitFirst().ShouldBeSome(expected);
    }
}