using Optional;
using Optional.Unsafe;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.Helpers
{
    public static class OptionExtensions
    {
        public static void ShouldBeSome<T>(this Option<T> option, T value)
        {
            option.AssertSome().ShouldBe(value);
        }

        // ReSharper disable once ParameterOnlyUsedForPreconditionCheck.Global
        public static void ShouldBeNone<T>(this Option<T> option)
        {
            Assert.False(option.HasValue);
        }

        public static T AssertSome<T>(this Option<T> option)
        {
            Assert.True(option.HasValue);
            return option.ValueOrDefault();
        }
    }
}