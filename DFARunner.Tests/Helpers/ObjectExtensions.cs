using System;

namespace DFARunner.Tests.Helpers
{
    public static class ObjectExtensions
    {
        public static void ShouldSatisfyAll<T>(this T t, params Action<T>[] actions)
        {
            foreach (var action in actions) action.Invoke(t);
        }
    }
}