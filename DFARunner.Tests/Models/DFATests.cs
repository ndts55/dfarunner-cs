using System.Collections.Immutable;
using System.IO;
using System.Linq;
using DFARunner.Models;
using DFARunner.Tests.Helpers;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.Models
{
    public class DfaTests
    {
        [Fact]
        public void DFA_accepts_word()
        {
            const string source = "init:q0;accept:q1;q0,a,q1;q1,a,q0";
            var dfa = new Parser().Parse(source);

            dfa.Accepts("aaaa").ShouldBeFalse();
            dfa.Accepts("aaa").ShouldBeTrue();
        }

        [Fact]
        public void DFA_invalid_when_initial_null()
        {
            var q0 = State.Create("q0").AssertSome();
            var qs = new[] {q0}.ToImmutableArray();
            var dfa = new Dfa(
                ImmutableHashSet<char>.Empty,
                qs, 
                qs, 
                null);

            dfa.IsValid.ShouldBeFalse();
        }

        [Fact]
        public void DFA_invalid_when_AcceptStates_empty()
        {
            var q0 = State.Create("q0").AssertSome();
            var qs = new[] {q0}.ToImmutableArray();
            var dfa = new Dfa(
                ImmutableHashSet<char>.Empty,
                qs,
                ImmutableArray<State>.Empty,
                q0);

            dfa.IsValid.ShouldBeFalse();
        }

        [Fact]
        public void DFA_invalid_when_States_empty()
        {
            var q0 = State.Create("q0").AssertSome();
            var qs = new[] {q0}.ToImmutableArray();
            new Dfa(
                    ImmutableHashSet<char>.Empty,
                    ImmutableArray<State>.Empty,
                    qs,
                    q0)
                .IsValid
                .ShouldBeFalse();
        }

        [Fact]
        public void DFA_invalid_when_Initial_not_in_States()
        {
            var q0 = State.Create("q0").AssertSome();
            var qs = new[] {State.Create("q2").AssertSome()}.ToImmutableArray();
            var aqs = new[] {q0}.ToImmutableArray();

            new Dfa(
                    ImmutableHashSet<char>.Empty,
                    qs,
                    aqs, 
                    q0)
                .IsValid
                .ShouldBeFalse();
        }

        [Fact]
        public void DFA_invalid_when_AcceptStates_not_in_States()
        {
            var q0 = State.Create("q0").AssertSome();
            var q1 = State.Create("q1").AssertSome();
            var q2 = State.Create("q2").AssertSome();
            var q3 = State.Create("q3").AssertSome();
            var qs = new[] {q0, q1, q2}.ToImmutableArray();
            var aqs = new[] {q3}.ToImmutableArray();

            new Dfa(
                    ImmutableHashSet<char>.Empty,
                    qs, 
                    aqs, 
                    q0)
                .IsValid
                .ShouldBeFalse();
        }

        [Fact]
        public void DFA_invalid_when_Transitions_not_deterministic()
        {
            var q0 = State.Create("q0").AssertSome();
            var qs = new[] {q0}.ToImmutableArray();

            var sigma = ImmutableHashSet<char>.Empty.Add('a');
            
            new Dfa(sigma, qs, qs, q0)
                .IsValid
                .ShouldBeFalse();
        }

        [Fact]
        public void DFA_from_example_is_valid()
        {
            var source = File.ReadAllText("../../../Examples/dfa01.dfa");
            new Parser()
                .Parse(source)
                .IsValid
                .ShouldBeTrue();
        }
    }
}