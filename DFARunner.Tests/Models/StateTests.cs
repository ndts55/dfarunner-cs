using System;
using System.Collections.Generic;
using DFARunner.Models;
using DFARunner.Tests.Helpers;
using Shouldly;
using Xunit;

namespace DFARunner.Tests.Models
{
    public class StateTests
    {
        // ReSharper disable once MemberCanBePrivate.Global
        public static readonly IEnumerable<object[]> CTestData =
            new[]
            {
                new object[] {string.Empty, false}, 
                new object[] {"q0", true},
                new object[] {"q;0", false},
                new object[] {"q:0", false},
                new object[] {"q1,q2", false},
                new object[] {"state_where_something-happens!", true}
            };

        [Theory]
        [MemberData(nameof(CTestData))]
        public void Create_returns_correct_option(string name, bool expectedHasValue)
        {
            State.Create(name).HasValue.ShouldBe(expectedHasValue);
        }

        [Fact]
        public void Overwriting_Transitions_should_throw_an_Exception()
        {
            var state = State.Create("q0").AssertSome();
            var q1 = State.Create("q1").AssertSome();
            var q2 = State.Create("q2").AssertSome();

            state.AddTransition('a', q1);
            Should.Throw<Exception>(() => state.AddTransition('a', q2));
        }

        [Fact]
        public void Same_states_should_be_equal()
        {
            var state = State.Create("q0").AssertSome();
            var same = state;

            state.ShouldBe(same);
        }

        [Fact]
        public void States_with_same_name_and_states_should_be_equal()
        {
            var state = State.Create("final").AssertSome();

            var s1 = State.Create("q0").AssertSome();
            s1.AddTransition('a', state);

            var s2 = State.Create("q0").AssertSome();
            s2.AddTransition('a', state);

            s1.Equals(s2).ShouldBeTrue();
        }

        [Fact]
        public void Transitions_should_be_empty_initially()
        {
            var state = State.Create("q0").AssertSome();
            state.Transitions.ShouldBeEmpty();
        }

        [Fact]
        public void Transitions_should_be_updated_when_adding_transition()
        {
            var q1 = State.Create("q1").AssertSome();
            var q2 = State.Create("q2").AssertSome();

            const char input = 'a';

            var expected = new Dictionary<char, State>
            {
                {input, q2}
            } as IReadOnlyDictionary<char, State>;

            q1.AddTransition(input, q2);

            q1.Transitions.ShouldBe(expected);
            q2.Transitions.ShouldBeEmpty();
        }

        [Fact]
        public void Compute_returns_self_on_empty_input()
        {
            var state = State.Create("q0").AssertSome();
            state.Compute(string.Empty).ShouldBeSameAs(state);
        }

        [Fact]
        public void Compute_returns_next_state_when_transition_present()
        {
            var state = State.Create("q0").AssertSome();
            var q2 = State.Create("q2").AssertSome();
            
            state.AddTransition('a', q2);
            
            state.Compute("a").ShouldBeSameAs(q2);
        }

        [Fact]
        public void Compute_throws_on_unexpected_input()
        {
            var state = State.Create("q0").AssertSome();
            
            Should.Throw<UnexpectedSymbolException>(() => state.Compute("b"))
                .Message
                .ShouldBe("unexpected symbol: b");
        }
    }
}